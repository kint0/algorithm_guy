
def bubble():
	objects = ["ant","apple","apply","bee","zoo","yerk","monkey","lion"]
	
	for i in range(len(objects)-1):
		swapped = False
		for j in range(len(objects)-i-1):
			print "i", i, "j", j
			if objects[j] > objects[j+1]:
			
				temp = objects[j]
				objects[j] = objects[j+1]
				objects[j+1] = temp
				swapped = True
		
		if swapped == False:
			break
	return objects

def binarySearch():
	objects = ['ant', 'apple', 'apply', 'bee', 'lion', 'monkey', 'yerk', 'zoo']
	target = 'yerk'
	left = 0
	right = len(objects)-1
	middle = 0
	answer = 0
	
	for i in range(len(objects)):
		middle = (right + left)/2
		if objects[middle] > target:
			right = middle
		elif objects[middle] < target:
			left = middle

		if objects[middle] == target:
			answer = middle
			break
		elif objects[left] == target:
			answer = left
			break
		elif objects[right] == target:
			answer = right
			break
		print left,middle,right

	return objects[answer],answer
	

if __name__ == "__main__":
	print bubble()
	print binarySearch()